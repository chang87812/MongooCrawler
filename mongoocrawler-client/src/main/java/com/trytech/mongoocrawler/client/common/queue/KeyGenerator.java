package com.trytech.mongoocrawler.client.common.queue;

/**
 * 获取全局map键的接口
 */
public interface KeyGenerator {

    public String getKey();
}
