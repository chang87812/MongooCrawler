package com.trytech.mongoocrawler.common.transport.event;

/**
 * Created by coliza on 2018/9/16.
 */
public interface CrawlerStateChangedListener extends IEventListener<CrawlerStateChangedEvent> {
}
