package com.trytech.mongoocrawler.server.common.queue;

import com.lmax.disruptor.RingBuffer;
import com.trytech.mongoocrawler.server.transport.http.WebResult;

/**
 * Created by hp on 2017-1-25.
 */
public class WebFetcherEventProducer extends FetcherEventProducer<WebResultFetcherEvent,WebResult<String>> {
    public WebFetcherEventProducer(RingBuffer<WebResultFetcherEvent> ringBuffer) {
        super(ringBuffer);
    }

    @Override
    public void sendData(WebResult<String> data) {
        System.out.println("发送了消息:"+data);
        super.sendData(data);
    }
}
